package com.example.demo.config;

import com.example.demo.service.GrpcService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class GrpcConfig {

    private final GrpcService grpcService;

    @Autowired
    public GrpcConfig(GrpcService grpcService) {
        this.grpcService = grpcService;
    }

    @Bean
    public void startGrpc() throws IOException, InterruptedException {

        Server server = ServerBuilder
                .forPort(8082)
                .addService(grpcService)
                .build();

        server.start();

        server.awaitTermination();

    }
}
