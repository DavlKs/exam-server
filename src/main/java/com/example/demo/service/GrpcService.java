package com.example.demo.service;

import com.example.MessageServerGrpc;
import com.example.Server;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GrpcService extends MessageServerGrpc.MessageServerImplBase {

    private static final Logger log = LoggerFactory.getLogger(GrpcService.class);

    public void getMessage(Server.GetMessageRequest request,
                              StreamObserver<Server.GetMessageResponse> responseObserver) {

        log.info(request.getMessage());

        Server.GetMessageResponse getCardNumberResponse = Server.GetMessageResponse
                .newBuilder()
                .build();

        responseObserver.onNext(getCardNumberResponse);

        responseObserver.onCompleted();

    }
}
